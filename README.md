# ELOG

This repository is a clone of the official PSI ELog from https://bitbucket.org/ritt/elog/src/master/

The branch **master** should be kept in sync with the PSI remote.
The branch **ess** is used to apply local modifications. It includes a ``.gitlab-ci.yml`` file that automatically builds a RPM on every push.
The RPM is only uploaded to artifactory on tag.

The tag is used as the version of the RPM.
