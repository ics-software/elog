/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E
  config.disableNativeSpellChecker = false;
  config.extraPlugins = 'timestamp';
  config.mathJaxLib = '../mathjax/MathJax.js?config=TeX-AMS_HTML';
  config.contentsCss = ['elog.css', 'default.css'];
};
